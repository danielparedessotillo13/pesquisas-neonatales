<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->date('diagnosed_at')->comment('Fecha del reesultado');
            $table->boolean('positive')->default(false)->comment('Resultado positivo o negativo');
            $table->text('pathology')->nullable()->comment('Patología');
            $table->text('levels')->nullable()->comment('Niveles');
            $table->enum('hc_tsh_result', ['N', 'S'])->nullable()
                  ->comment('Resultado HC (TSH): (N)ormal, (S)suspicious');
            $table->float('hc_tsh_value')->nullable()->comment('Valor del resultado HC (TSH)');

            $table->enum('hac_hp_result', ['N', 'S'])->nullable()
                  ->comment('Resultado HAC (170HP): (N)ormal, (S)suspicious');
            $table->float('hac_hp_value')->nullable()->comment('Valor del resultado HAC (170HP)');

            $table->enum('pku_phe_result', ['N', 'S'])->nullable()
                  ->comment('Resultado PKU (PHE): (N)ormal, (S)suspicious');
            $table->float('pku_phe_value')->nullable()->comment('Valor del resultado PKU (PHE)');

            $table->enum('gal_gt_result', ['N', 'S'])->nullable()
                  ->comment('Resultado GAL (GT): (N)ormal, (S)suspicious');
            $table->float('gal_gt_value')->nullable()->comment('Valor del resultado GAL (GT)');

            $table->enum('db_b_result', ['N', 'S'])->nullable()
                  ->comment('Resultado DB (B): (N)ormal, (S)suspicious');
            $table->float('db_b_value')->nullable()->comment('Valor del resultado DB (B)');

            $table->enum('fq_irt_result', ['N', 'S'])->nullable()
                  ->comment('Resultado FQ (IRT): (N)ormal, (S)suspicious');
            $table->float('fq_irt_value')->nullable()->comment('Valor del resultado FQ (IRT)');

            $table->foreignId('medical_record_id')->constrained()->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
