<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('type', ['ADM', 'ANT', 'TRN', 'BNT', 'PCT'])->default('PCT')->comment(
                'Tipos de Usuarios: ADM = Administrador, ANT = Analista, TRN = Transcriptor, BNT = Bioanalista, PCT = Paciente'
            );
            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('last_login')->nullable()->comment('Último acceso registrado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
