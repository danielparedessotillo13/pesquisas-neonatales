<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Gender;
use DB;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            Gender::updateOrCreate(['name' => 'Masculino'], []);
            Gender::updateOrCreate(['name' => 'Femenino'], []);
        });
    }
}
