<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;
use DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('123456');
        $users = [
            [
                'userFilter' => ['username' => 'admin'],
                'userData' => [
                    'email' => 'admin@mail.com',
                    'type' => 'ADM',
                    'password' => $password
                ],
                'profile' => [
                    'identity_card' => 'X00000000',
                    'names' => 'admin',
                    'surnames' => 'admin'
                ]
            ],
            [
                'userFilter' => ['username' => 'analist'],
                'userData' => [
                    'email' => 'analist@mail.com',
                    'type' => 'ANT',
                    'password' => $password
                ],
                'profile' => [
                    'identity_card' => 'X00000001',
                    'names' => 'analista',
                    'surnames' => 'analista'
                ]
            ],
            [
                'userFilter' => ['username' => 'transc'],
                'userData' => [
                    'email' => 'transc@mail.com',
                    'type' => 'TRN',
                    'password' => $password
                ],
                'profile' => [
                    'identity_card' => 'X00000002',
                    'names' => 'transcriptor',
                    'surnames' => 'transcriptor'
                ]
            ],
            [
                'userFilter' => ['username' => 'bioanalist'],
                'userData' => [
                    'email' => 'bioanalist@mail.com',
                    'type' => 'BNT',
                    'password' => $password
                ],
                'profile' => [
                    'identity_card' => 'X00000003',
                    'names' => 'bioanalista',
                    'surnames' => 'bioanalista'
                ]
            ],
            [
                'userFilter' => ['username' => 'patient'],
                'userData' => [
                    'email' => 'patient@mail.com',
                    'type' => 'PCT',
                    'password' => $password
                ],
                'profile' => [
                    'identity_card' => 'X00000004',
                    'names' => 'paciente',
                    'surnames' => 'paciente'
                ]
            ],
        ];
        DB::transaction(function () use ($users) {
            foreach ($users as $user) {
                /** @var Object Datos del usuario */
                $usr = User::updateOrCreate($user['userFilter'], $user['userData']);
                if ($usr->profile()->first()) {
                    $usr->profile()->update($user['profile']);
                } else {
                    $usr->profile()->create($user['profile']);
                }
            }
        });
    }
}
