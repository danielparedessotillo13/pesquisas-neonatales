/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import {ServerTable, ClientTable, Event} from 'vue-tables-2';
import 'sweetalert2/src/sweetalert2.scss';
window.Swal = require('sweetalert2');
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(ServerTable);

Vue.mixin({
    data() {
        return {
            csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            table_options: {
                highlightMatches: true,
                perPage:10,
                perPageValues:[10, 20, 50],
                sortable: true,
                filterable: true,
                orderBy: false,
                columnsDropdown: false,
                dateFormat: "DD/MM/YYYY",
                pagination: {
                    show:true,
                    dropdown: false,
                    chunk: 10,
                    edge: true,
                    align: "right",
                    nav: "fixed"
                },
                texts: {
                    filter: "Buscar:",
                    filterBy: 'Buscar por {column}',
                    //count:'Página {page}',
                    count: ' ',
                    first: 'PRIMERO',
                    last: 'ÚLTIMO',
                    limit: 'Registros',
                    //page: 'Página:',
                    loadingError: 'Oops! No se pudo cargar la información',
                    noResults: 'No existen registros',
                    loading: "Cargando...",
                    filterPlaceholder: "Buscar...",
                },
                sortIcon: {
                    is: 'fa-sort cursor-pointer',
                    base: 'fa',
                    up: 'fa-sort-up cursor-pointer',
                    down: 'fa-sort-down cursor-pointer'
                },
            },
        }
    },
    methods: {
        /**
         * Establece la ruta de un archivo
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}       file    Ubicación del archivo
         */
        setPathFile(file) {
            return `${window.url}/${file}`;
        },
        /**
         * Establece una URL
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}    route    Ruta
         */
        setUrl(route) {
            return `${window.url}/${route}`;
        },
        /**
         * Obtiene los datos de un registro
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}     url    URL a consultar para obtener los datos del registro
         */
        async getRecord(url) {
            const vm = this;
            await axios.get(url).then(response => {
                vm.record = response.data.result;
            }).catch(error => {
                console.error(error);
            });
        },
        /**
         * Obtiene un arreglo de registros solicitados
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}      url    URL a consultar
         */
        async getRecords(url) {
            const vm = this;
            
            await axios.get(url).then(response => {
                vm.records = response.data.result;
            }).catch(error => {
                console.error(error);
            });
        },
        /**
         * Realiza el proceso para registrar o actualizar datos
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}        url        URL encargada de establecer los datos
         * @param     {String}        urlBack    URL a mostrar una vez establecida la información. Opcional
         */
        async setRecord(url, urlBack = null) {
            const vm = this;
            
            if (typeof(vm.record.id)!=="undefined") {
                // Update record
                await axios.put(
                    `${url}/${vm.record.id}`, vm.record
                ).then(response => {
                    if (response.data.code === 200) {
                        vm.showMessage('updated');
                        if (urlBack) {
                            vm.$router.push(urlBack);
                        }
                    }
                }).catch(error => {
                    vm.setErrors(error);
                });
            } else {
                // Create record
                await axios.post(`${url}`, vm.record).then(response => {
                    if (response.data.code === 200) {
                        vm.showMessage('success');
                        if (urlBack) {
                            vm.$router.push(urlBack);
                        }
                    }
                }).catch(error => {
                    vm.setErrors(error);
                });
            }
        },
        /**
         * Establece los datos del registro a modificar
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}      url     URL para el registro a editar
         * @param     {String}      id      ID del registro a editar
         */
        editRecord(url, id) {
            this.$router.push(`${url}/${id}`);
        },
        /**
         * Elimina el reistro solicitado
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}        url     URL para la eliminación del registro
         * @param     {Integer}       id      Identificador del registro a eliminar
         */
        deleteRecord(url, id) {
            const vm = this;
            Swal.fire({
                title: '¿Estás seguro que quieres eliminar este registro?',
                text: 'Esta acción es definitiva y el registro no podrá recuperarse!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Sí',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(`${url}/${id}`).then(response => {
                        if (response.data.code === 200) {
                            vm.getRecords(url);
                            vm.showMessage('destroy');
                        }
                    }).catch(error => {
                        console.error(error);
                        vm.setErrors(error);
                    });
                }
            });
        },
        /**
         * Establece información de errores generados en el sistema
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {Object}     error    Objeto con infrmación de los errores
         */
        setErrors(error) {
            let errors = [];
            
            if (typeof(error.response) !="undefined") {
                if (typeof(error.response.data) !== "undefined" && typeof(error.response.data.errors) !== "undefined") {
                    for (var index in error.response.data.errors) {
                        if (error.response.data.errors[index]) {
                            errors.push(error.response.data.errors[index][0]);
                        }
                    }
                } else {
                    errors.push((window.debug)?error.response.data.message:'Error procesando la petición');
                }
                this.errors = errors;
            }
        },
        /**
         * Muestra un mensaje al usuario de acuerdo a la acción ejecutada
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}       type     Tipo de mensaje a mostrar
         *
         * @return    {[type]}       [description]
         */
        showMessage(type) {
            let options = {
                closeButton: true, progressBar: true, timeOut: 5000, extendedTimeOut: 1000,
                preventDuplicates:true
            };
            if (type === 'success') {
                toastr.success("Registro creado", "", options);
            } else if (type === 'updated') {
                toastr.success("Registro actualizado", "", options);
            } else if (type === 'destroy') {
                toastr.success("Registro eliminado", "", options);
            }
        },
        /**
         * Obtiene listado de registros
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {String}    url      URL de la petición
         * @param     {String}    field    Nombre del campo al cual asignar los valores obtenidos
         */
        async getList(url, field) {
            const vm = this;
            await axios.get(url).then(response => {
                vm[field] = response.data.result;
            }).catch(error => {
                console.log(error);
            });
        },
        /**
         * Determina si el usuario autenticado tiene acceso al módulo o registro a evaluar
         *
         * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
         *
         * @param     {Object}     user            Objeto con información del usuario autenticado
         * @param     {Array}      userType        Tipo de usuario
         *
         * @return    {Boolean}    Devuelve verdadero si el usuario tiene acceso, de lo contrario devuelve falso
         */
        checkRole(user, userType) {
            return userType.includes(user.role);
        },
        async getOrganisms() {
            const vm = this;
            let stateId = (typeof(vm.record.state_id)!=='undefined' && vm.record.state_id) ? vm.record.state_id : '';
            await axios.get(`${window.url}/api/v1/list/organisms/${stateId}`).then(response => {
                if (response.data.code===200) {
                    vm.organisms = response.data.result;
                }
            }).catch(error => {
                console.error(error);
            });
        }
    }
});

Vue.component('sidebar-component', require('./components/SidebarComponent.vue').default);
Vue.component('navbar-component', require('./components/NavbarComponent.vue').default);
Vue.component('footer-component', require('./components/FooterComponent.vue').default);
Vue.component('alert-component', require('./components/AlertComponent.vue').default);
Vue.component('message-component', require('./components/MessageComponent.vue').default);
Vue.component('form-errors', require('./components/FormErrorsComponent.vue').default);

import App from './views/App';
import Genders from './views/settings/Genders';
import GenderForm from './views/settings/GenderForm';
import States from './views/settings/States';
import StateForm from './views/settings/StateForm';
import Cities from './views/settings/Cities';
import CityForm from './views/settings/CityForm';
import Diseases from './views/settings/Diseases';
import DiseaseForm from './views/settings/DiseaseForm';
import Users from './views/auth/Users';
import UserForm from './views/auth/UserForm';
import ProfileForm from './views/auth/ProfileForm';
import Patients from './views/tests/Patients';
import PatientForm from './views/tests/PatientForm';
import Results from './views/tests/Results';
import ResultForm from './views/tests/ResultForm';
import Organisms from './views/settings/Organisms';
import OrganismForm from './views/settings/OrganismForm';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/genders',
            name: 'genders',
            component: Genders
        },
        {
            path: '/new/genders',
            name: 'genders-new',
            component: GenderForm
        },
        {
            path: '/genders/:id',
            name: 'genders-edit',
            component: GenderForm
        },
        {
            path: '/states',
            name: 'states',
            component: States
        },
        {
            path: '/new/states',
            name: 'states-new',
            component: StateForm
        },
        {
            path: '/states/:id',
            name: 'states-edit',
            component: StateForm
        },
        {
            path: '/cities',
            name: 'cities',
            component: Cities
        },
        {
            path: '/new/cities',
            name: 'cities-new',
            component: CityForm
        },
        {
            path: '/cities/:id',
            name: 'cities-edit',
            component: CityForm
        },
        {
            path: '/diseases',
            name: 'diseases',
            component: Diseases
        },
        {
            path: '/new/diseases',
            name: 'diseases-new',
            component: DiseaseForm
        },
        {
            path: '/diseases/:id',
            name: 'diseases-edit',
            component: DiseaseForm
        },
        {
            path: '/users',
            name: 'users',
            component: Users
        },
        {
            path: '/new/users',
            name: 'users-new',
            component: UserForm
        },
        {
            path: '/users/:id',
            name: 'users-edit',
            component: UserForm
        },
        {
            path: '/profile/users',
            name: 'users-profile',
            component: ProfileForm
        },
        {
            path: '/patients',
            name: 'patients',
            component: Patients
        },
        {
            path: '/new/patients',
            name: 'patients-new',
            component: PatientForm
        },
        {
            path: '/patients/:id',
            name: 'patients-edit',
            component: PatientForm
        },
        {
            path: '/results',
            name: 'results',
            component: Results
        },
        {
            path: '/new/results',
            name: 'results-new',
            component: ResultForm
        },
        {
            path: '/results/:id',
            name: 'results-edit',
            component: ResultForm
        },
        {
            path: '/organisms',
            name: 'organisms',
            component: Organisms
        },
        {
            path: '/new/organisms',
            name: 'organisms-new',
            component: OrganismForm
        },
        {
            path: '/organisms/:id',
            name: 'organisms-edit',
            component: OrganismForm
        },
    ]
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
