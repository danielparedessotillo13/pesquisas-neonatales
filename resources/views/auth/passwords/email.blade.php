@extends('layouts.auth')

@section('content')
    <div class="p-5">
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">{{ __('Restablecer contraseña') }}</h1>
        </div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group">
                <input type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" 
                       value="{{ old('email') }}" placeholder="{{ __('Correo Electrónico') }}" required 
                       autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button class="btn btn-primary btn-block">
                {{ __('Enviar') }}
            </button>
        </form>
        <hr>
        <div class="text-center">
            <a class="small" href="{{ route('login') }}">
                {{ __('Ir al formulario de acceso') }}
            </a>
        </div>
    </div>
@endsection
