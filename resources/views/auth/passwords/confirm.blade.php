@extends('layouts.auth')

@section('content')
    <div class="p-5">
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">{{ __('Confirmar contraseña') }}</h1>
        </div>
        {{ __('Confirme su contraseña antes de continuar.') }}
        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div class="form-group">
                <input type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" 
                       name="password" placeholder="{{ __('Contraseña') }}" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <button class="btn btn-primary btn-block">
                {{ __('Confirmar contraseña') }}
            </button>
        </form>
        @if (Route::has('password.request'))
            <hr>
            <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">
                    {{ __('¿Olvidó la contraseña?') }}
                </a>
            </div>
        @endif
    </div>
@endsection
