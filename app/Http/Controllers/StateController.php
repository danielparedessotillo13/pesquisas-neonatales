<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\Request;
use DB;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::all();

        return $this->success($states);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => ['required', 'min:2', 'max:2', 'unique:states,code'],
            'name' => ['required']
        ]);

        $state = DB::transaction(function () use ($request) {
            $state = State::create($request->input());
            return $state;
        });

        return $this->success($state);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        return $this->success($state);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        $this->validate($request, [
            'code' => ['required', 'min:2', 'max:2', 'unique:states,code,'.$state->id],
            'name' => ['required']
        ]);

        $state = DB::transaction(function () use ($request, $state) {
            $state->update($request->input());
            return $state;
        });

        return $this->success($state);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        DB::transaction(function () use ($state) {
            $state->delete();
        });

        return $this->success([]);
    }

    public function list()
    {
        $states = State::all();

        return $this->success($states);
    }
}
