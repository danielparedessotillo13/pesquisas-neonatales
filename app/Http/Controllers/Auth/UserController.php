<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$users = User::where('id', '<>', auth()->user()->id)->get();
        $users = User::with('profile')->get();
        return $this->success($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'type' => ['required'],
            'username' => ['required'],
            'email' => ['required', 'email']
        ];
        $requiredProfile = false;
        if ($request->profile['identity_card'] || $request->profile['names'] || $request->profile['surnames']) {
            $rules['profile.identity_card'] = ['required'];
            $rules['profile.names'] = ['required'];
            $rules['profile.surnames'] = ['required'];
            $requiredProfile = true;
        }
        $this->validate($request, $rules);

        $user = DB::transaction(function () use ($request, $requiredProfile) {
            $user = new User;
            $user->type = $request->type;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->organism_id = $request->organism_id ?? null;
            $password = $this->generateHash(12);
            $user->password = Hash::make($password);
            $user->save();
            if ($requiredProfile) {
                $user->profile()->create($request->profile);
            }
            try {
                $user->notify(new UserCreated($password));
            } catch (\Swift_TransportException $e) {
                if ($e->getCode() == '503') {
                    return $this->fail('failure', 'Error enviando notificación por correo ', $e->getCode());
                }
            }
            return $user;
        });

        return $this->success($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $this->success($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'type' => ['required'],
            'username' => ['required'],
            'email' => ['required', 'email']
        ];
        $requiredProfile = false;
        if ($request->profile['identity_card'] || $request->profile['names'] || $request->profile['surnames']) {
            $rules['profile.identity_card'] = ['required'];
            $rules['profile.names'] = ['required'];
            $rules['profile.surnames'] = ['required'];
            $requiredProfile = true;
        }
        $this->validate($request, $rules);

        $user = DB::transaction(function () use ($user, $request, $requiredProfile) {
            $user->update($request->input());
            if ($requiredProfile) {
                $user->profile->update($request->profile);
            }
            return $user;
        });

        return $this->success($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::transaction(function () use ($user) {
            $user->delete();
        });
        return $this->success([]);
    }

    /**
     * Actualiza los datos de perfil del usuario autenticado
     * 
     * @author     Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
     * 
     * @return JsonResponse
     */
    public function updateProfile(Request $request)
    {
        if ($request->password !== null || $request->password_confirmation !== null) {
            $this->validate($request, [
                'password' => ['required', 'string', 'min:8', 'confirmed']
            ]);
        }

        $user = DB::transaction(function () use ($request) {
            $user = User::find($request->id);
            if ($user) {
                $user->email = $request->email;
                if ($request->password !== null) {
                    $user->password = Hash::make($request->password);
                }
                $user->save();
                $user->profile->update($request->profile);
            }
            return $user;
        });

        return $this->success($user);
    }

    /**
     * Genera una cadena aleatoria
     *
     * @author     Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
     *
     * @param      integer          $length          Longitud de la cadena a generar
     * @param      boolean          $specialChars    Condición que determina si se incluyen o no carácteres especiales
     * @param      boolean          $separators      Condición que determina si se incluyen carácteres "-" y "_" como
     *                                               separadores de la cadena generada
     *
     * @return     string           Devuelve una cadena aleatoria
     */
    function generateHash($length = 8, $specialChars = false, $separators = false)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        $chars = '';

        if ($specialChars) {
            $chars = '%$[](-_)@/#{}';
            $alphabet .= $chars;
        }
        if ($separators) {
            $chars = '-_';
            $alphabet .= $chars;
        }
        $pass = [];
        $alphaLength = strlen($alphabet) - 1;
        $i = 0;
        while ($i < $length) {
            $n = rand(0, $alphaLength);
            if (in_array($alphabet[$n], $pass) && !empty($chars) && strpos($chars, $alphabet[$n])) {
                continue;
            }
            $pass[] = $alphabet[$n];
            $i++;
        }

        $hash = implode($pass);
        return $hash;
    }
}
