<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use App\Models\Mother;
use Illuminate\Http\Request;
use DB;

class PatientController extends Controller
{
    public $validateMessages;

    public function __construct()
    {
        $this->validateMessages = [
            'register_number.required' => 'El campo número de registro es obligatorio.',
            'register_number.unique' => 'El campo número de registro ya se encuentra registrado',
            'birthdate.required' => 'El campo fecha de nacimiento del paciente es obligatorio.',
            'birthdate.date' => 'El campo fecha de nacimiento del paciente no tiene un formato valido.',
            'first_surname.required' => 'El campo Primer Apellido del paciente es obligatorio.',
            'second_surname.required' => 'El campo Segundo Apellido del paciente es obligatorio.',
            'first_name.required' => 'El campo Primer nombre del paciente es obligatorio.',
            'middle_name.required' => 'El campo inicial del segundo nombre es obligatorio.',
            'mother.identity_card.required' => 'El campo cédula de la madre es obligatorio.',
            'mother.identity_card.max' => 'El campo cédula debe ser de hasta un máximo de 10 carácteres.',
            'mother.birtdate.required' => 'El campo fecha de nacimiento de la madre es obligatorio.',
            'mother.birthdate.date' => 'El campo de fecha de nacimiento de la madre no tiene un formato valido.',
            'mother.first_surname.required' => 'El campo primer apellido de la madre es obligatorio.',
            'mother.second_surname.required' => 'El campo segundo apellido de la madre es obligatorio.',
            'mother.first_name.required' => 'El campo primer nombre de la madre es obligatorio.',
            'mother.middle_name.required' => 'El campo inicial del segundo nombre de la madre es obligatorio.',
            'medical_record.number.required' => 'El campo número de la historia clinica es obligatorio.',
            'medical_record.number.unique' => 'El campo número de la historia clinica ya se encuentra registrado.',
            'medical_record.sample_taken_at.required' => 'El campo toma de la muestra de la historia clinica es obligatorio.',
            'medical_record.sample_taken_at.date' => 'El campo toma de la muestra de la historia clinica tiene un formato inválido.',
            'medical_record.birth_weight.required' => 'El campo peso al nacer de la historia clinica es obligatorio.',
            'medical_record.pregnancy_duration.required' => 'El campo duración de embarazo de la historia clinica es obligatorio.',
            'medical_record.pregnancy_duration.integer' => 'El campo duración de embarazo debe ser un número entero.',
            'medical_record.birth_type.required' => 'El campo tipo de parto de la historia clinica es obligatorio.',
            'medical_record.consanguineous_parents.required' => 'El campo padres consanguíneos de la historia clinica es obligatorio.',
            'medical_record.lactation_type.required' => 'El campo tipo de lactancia de la historia clinica es obligatorio.',

        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::with('mother')->get();
        return $this->success($patients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'register_number' => ['required', 'unique:patients,register_number'],
            'birthdate' => ['required', 'date'],
            'first_surname' => ['required'],
            'second_surname' => ['required'],
            'first_name' => ['required'],
            'middle_name' => ['required'],
            'mother.identity_card' => ['required', 'max:10'],
            'mother.birthdate' => ['required', 'date'],
            'mother.first_surname' => ['required'],
            'mother.second_surname' => ['required'],
            'mother.first_name' => ['required'],
            'mother.middle_name' => ['required'],
            'medical_record.number' => ['required', 'unique:medical_records,number'],
            'medical_record.sample_taken_at' => ['required', 'date'],
            'medical_record.birth_weight' => ['required'],
            'medical_record.pregnancy_duration' => ['required', 'integer'],
            'medical_record.birth_type' => ['required'],
            'medical_record.consanguineous_parents' => ['required'],
            'medical_record.lactation_type' => ['required']
        ], $this->validateMessages);

        $patient = DB::transaction(function () use ($request) {
            $mother = Mother::where('identity_card', $request->mother['identity_card'])->first();
            if (!$mother) {
                $mother = Mother::create($request->mother);
            }
            $patient = $mother->patients()->create($request->input());
            $patient->medicalRecord()->create($request->medical_record);

            return $patient;
        });

        return $this->success($patient);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return $this->success($patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $this->validate($request, [
            'register_number' => ['required', 'unique:patients,register_number,' . $patient->id],
            'birthdate' => ['required', 'date'],
            'first_surname' => ['required'],
            'second_surname' => ['required'],
            'first_name' => ['required'],
            'middle_name' => ['required'],
            'mother.identity_card' => ['required', 'max:10'],
            'mother.birthdate' => ['required', 'date'],
            'mother.first_surname' => ['required'],
            'mother.second_surname' => ['required'],
            'mother.first_name' => ['required'],
            'mother.middle_name' => ['required'],
            'medical_record.number' => ['required', 'unique:medical_records,number,' . $patient->medicalRecord->id],
            'medical_record.sample_taken_at' => ['required', 'date'],
            'medical_record.birth_weight' => ['required'],
            'medical_record.pregnancy_duration' => ['required', 'integer'],
            'medical_record.birth_type' => ['required'],
            'medical_record.consanguineous_parents' => ['required'],
            'medical_record.lactation_type' => ['required']
        ], $this->validateMessages);

        $patient = DB::transaction(function () use ($request, $patient) {
            $patient->update($request->input());
            $patient->medicalRecord()->update($request->medical_record);

            return $patient;
        });

        return $this->success($patient);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        DB::transaction(function () use ($patient) {
            $patient->medicalRecord()->delete();
            $patient->delete();
        });
        return $this->success([]);
    }

    public function searchMother(Request $request)
    {
        $mother = Mother::where('identity_card', $request->identity_card)->first();

        return $this->success($mother);
    }

    public function search(Request $request)
    {
        $patient = Patient::with(['medicalRecord', 'mother'])
                          ->where('register_number', $request->register_number)->first();

        $empty = [
            'diagnosed_at' => '',
            'pathology' => '',
            'levels' => '',
            'positive' => false,
            'hc_tsh_result' => '',
            'hc_tsh_value' => '',
            'hac_hp_result' => '',
            'hac_hp_value' => '',
            'pku_phe_result' => '',
            'pku_phe_value' => '',
            'gal_gt_result' => '',
            'gal_gt_value' => '',
            'db_b_result' => '',
            'db_b_value' => '',
            'fq_irt_result' => '',
            'fq_irt_value' => '',
            'medical_record' => [
                'id' => '',
                'patient' => [
                    'register_number' => '',
                ]
            ],
            'mother' => [
                'identity_card' => '',
                'birthdate' => '',
                'first_surname' => '',
                'second_surname' => '',
                'first_name' => '',
                'middle_name' => ''
            ]
        ];

        return $this->success($patient ?? $empty);
    }
}
