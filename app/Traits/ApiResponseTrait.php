<?php
namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

trait ApiResponseTrait
{
    /**
     * Valida los datos de la petición
     *
     * @method    validator
     *
     * @author    Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param     Object       $request             Datos de la petición
     * @param     Array        $validations         Arreglo con las reglas de validación
     * @param     Array        $responseMessages    Arreglo con los mensajes de validación a mostrar
     *
     * @return    Boolean      Devuelve verdadero si la validación es exitosa, de lo contrario devuelve falso
     */
    protected function validator($request, $validations, $responseMessages = [])
    {
        $validator = Validator::make($request->all(), $validations, $responseMessages);

        if ($validator->fails()) {
            return false;
        }

        return true;
    }

    /**
     * Mensaje de error cuando la petición falla
     *
     * @method     fail
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      string|null      $status     Descripción del estatus
     * @param      string|null      $message    Mensaje de error sobre la petición
     * @param      array            $errors     Arreglo con los errores generados
     * @param      integer          $code       Código de error generado
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function fail($status = null, $message = null, $errors = [], $code = 500)
    {
        return response()->json([
            'code' => $code,
            'status' => $status ?? __('Failure'),
            'message' => ($message !== null) ? $message : __('Internal Server Error'),
            'result' => ['errors' => $errors]
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Mensaje de error para cuando la validación de campos falla
     *
     * @method    validationFail
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param     Object            $validator    Objeto con información de la validación
     * @param     String|null       $message      Mensaje a mostrar
     * @param     integer           $code         Código del eststus de respuesta
     *
     * @return    JsonResponse      Objeto con información de rspuesta a la petición de validación
     */
    protected function validationFail($validator, $message = null, $code = 422)
    {
        $errors = [];
        foreach ($validator->errors()->getMessages() as $input => $msg) {
            $errors[$input] = $msg;
        }

        return response()->json([
            'code' => $code,
            'status' => __('Validation failure'),
            'message' => ($message !== null) ? $message : __('Invalid information'),
            'result' => ['errors' => $errors]
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Mensaje de petición correcta
     *
     * @method     success
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      array|null       $data    Datos devueltos en la petición
     * @param      integer          $code    Código del estatus de la petición
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function success($data, $message = null, $code = 200)
    {
        return response()->json([
            'code' => $code,
            'status' => __('success'),
            'message' => ($message !== null) ? $message : JsonResponse::$statusTexts[$code] = __('OK'),
            'result' => $data,
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_OK);
    }

    /**
     * Mensaje para peticiones incorrectas
     *
     * @method     badRequest
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      string|null      $message    Mensaje de la petición
     * @param      integer          $code       Código del estatus de la petición
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function badRequest($message, $code = 400)
    {
        return response()->json([
            'code' => $code,
            'status' => __('Bad Request'),
            'message' => $message ? $message : __('Bad Request'),
            'result' => []
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * Mensaje para peticiones que no encuentran el recurso solicitado
     *
     * @method     notFound
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      string|null      $message    Mensaje de la petición
     * @param      integer          $code       Código del estatus de la petición
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function notFound($message, $code = 404)
    {
        return response()->json([
            'code' => $code,
            'status' => __('Not Found'),
            'message' => $message ? $message : __('Not Found'),
            'result' => []
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_NOT_FOUND);
    }

    /**
     * Mensaje para peticiones que no disponen de acceso al recurso solicitado
     *
     * @method     forbidden
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      string|null      $message    Mensaje de la petición
     * @param      integer          $code       Código del estatus de la petición
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function forbidden($message, $code = 403)
    {
        return response()->json([
            'code' => $code,
            'status' => __('Forbidden'),
            'message' => $message ? $message : __('Forbidden'),
            'result' => []
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_FORBIDDEN);
    }

    /**
     * Mensaje para peticiones no autorizadas
     *
     * @method     unauthorized
     *
     * @author     Ing. Roldan Vargas <roldandvg@gmail.com>
     *
     * @param      string|null      $message    Mensaje de la petición
     * @param      integer          $code       Código del esttaus de la petición
     *
     * @return     JsonResponse     Objeto JSON con información sobre la petición solicitada
     */
    protected function unauthorized($message, $code = 401)
    {
        return response()->json([
            'code' => $code,
            'status' => __('Unauthorized'),
            'message' => $message ? $message : __('Unauthorized'),
            'result' => []
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_UNAUTHORIZED);
    }
}
