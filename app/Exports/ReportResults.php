<?php

namespace App\Exports;

use App\Models\Result;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportResults implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    protected $fromDate;
    protected $toDate;

    public function __construct($fromDate, $toDate)
    {
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    public function headings(): array
    {
        return [
            'Nro Registro',
            'Apellidos y Nombres del Paciente',
            'Fecha Nacimiento',
            'Fecha Muestra',
            'Sexo',
            'Nombre de la Madre',
            'C.I. Madre',
            'Resultado HC (TSH)',
            'Resultado HAC (170HP)',
            'Resultado PKU (PHE)',
            'Resultado GAL (GT)',
            'Resultado DB (B)',
            'Resultado FQ (IRT)'
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $results = Result::with(['medicalRecord' => function ($q) {
            $q->with('patient', function ($qq) {
                $qq->with('mother');
            });
        }]);

        if ($this->fromDate !== null) {
            $results = $results->whereDate('diagnosed_at', '>=', $this->fromDate);
        }
        if ($this->toDate !== null) {
            $results = $results->whereDate('diagnosed_at', '<=', $this->toDate);
        }
        
        $results = $results->get();

        return $results;
    }

    public function map($result): array
    {
        return [
            $result->medicalRecord->patient->register_number,
            $result->medicalRecord->patient->full_name,
            $result->medicalRecord->patient->birthdate->format('d-m-Y'),
            $result->diagnosed_at->format('d-m-Y'),
            ($result->medicalRecord->patient->sex === 'M')?'Masculino':'Femenino',
            $result->medicalRecord->patient->mother->full_name,
            $result->medicalRecord->patient->mother->identity_card,
            $result->hc_tsh_diagnose,
            $result->hac_hp_diagnose,
            $result->pku_phe_diagnose,
            $result->gal_gt_diagnose,
            $result->db_b_diagnose,
            $result->fq_irt_diagnose
        ];
    }
}
