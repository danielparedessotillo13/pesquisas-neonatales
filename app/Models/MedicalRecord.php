<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalRecord extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'sample_taken_at', 'first_sample', 'second_sample', 'birth_weight', 'pregnancy_duration',
        'take_antibiotics', 'background', 'birth_type', 'consanguineous_parents', 'lactation_type', 
        'patient_id'
    ];

    protected $casts = [
        'sample_taken_at' => 'date'
    ];

    /**
     * MedicalRecord belongs to Patient.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * MedicalRecord has one Result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function result()
    {
        return $this->hasOne(Result::class);
    }
}
