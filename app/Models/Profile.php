<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['identity_card', 'names', 'surnames', 'user_id'];

    protected $appends = ['full_name'];

    /**
     * Profile belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Obtiene nombre completo del perfil de usuario
     *
     * @method    getFullNameAttribute
     *
     * @author    Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
     *
     * @return    string                  Devuelve el nombre completo del perfil de usuario
     */
    public function getFullNameAttribute()
    {
        return trim($this->names . ' ' . $this->surnames);
    }
}
