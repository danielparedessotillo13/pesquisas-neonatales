<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'register_number', 'first_surname', 'second_surname', 'first_name', 'middle_name', 'birthdate', 'sex',
        'mother_id'
    ];

    protected $appends = ['full_name'];

    protected $with = ['mother', 'medicalRecord'];

    protected $casts = [
        'birthdate' => 'date:Y-m-d'
    ];

    protected $dates = ['birthdate'];

    public function getFullNameAttribute()
    {
        $fullName = $this->first_surname . ' ' . $this->second_surname . ' ' . $this->first_name . ' ' . 
                    $this->middle_name;
        return trim($fullName);
    }

    /**
     * Patient belongs to Mother.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mother()
    {
        return $this->belongsTo(Mother::class);
    }

    /**
     * Patient has one MedicalRecord.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function medicalRecord()
    {
        return $this->hasOne(MedicalRecord::class);
    }
}
