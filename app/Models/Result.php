<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'diagnosed_at', 'positive', 'pathology', 'levels', 'hc_tsh_result', 'hc_tsh_value',
        'hac_hp_result', 'hac_hp_value', 'pku_phe_result', 'pku_phe_value', 'gal_gt_result',
        'gal_gt_value', 'db_b_result', 'db_b_value', 'fq_irt_result', 'fq_irt_value', 'medical_record_id'
    ];

    protected $casts = [
        'diagnosed_at' => 'date:Y-m-d'
    ];

    protected $dates = ['diagnosed_at'];

    protected $append = [
        "hc_tsh_diagnose", "hac_hp_diagnose", "pku_phe_diagnose", "gal_gt_diagnose",
        "db_b_diagnose", "fq_irt_diagnose"
    ];

    /**
     * Result belongs to MedicalRecord.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medicalRecord()
    {
        return $this->belongsTo(MedicalRecord::class);
    }

    public function getHcTshDiagnoseAttribute()
    {
        $result = '';
        if ($this->hc_tsh_result !== null) {
            if ($this->hc_tsh_result === 'N') {
                $result .= "Normal";
            } elseif ($this->hc_tsh_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->hc_tsh_value !== null) {
            $result .= " ($this->hc_tsh_value)";
        }

        return $result;
    }

    public function getHacHpDiagnoseAttribute()
    {
        $result = '';
        if ($this->hac_hp_result !== null) {
            if ($this->hac_hp_result === 'N') {
                $result .= "Normal";
            } elseif ($this->hac_hp_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->hac_hp_value !== null) {
            $result .= " ($this->hac_hp_value)";
        }
        
        return $result;
    }

    public function getPkuPheDiagnoseAttribute()
    {
        $result = '';
        if ($this->pku_phe_result !== null) {
            if ($this->pku_phe_result === 'N') {
                $result .= "Normal";
            } elseif ($this->pku_phe_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->pku_phe_value !== null) {
            $result .= " ($this->pku_phe_value)";
        }
        
        return $result;
    }

    public function getGalGtDiagnoseAttribute()
    {
        $result = '';
        if ($this->gal_gt_result !== null) {
            if ($this->gal_gt_result === 'N') {
                $result .= "Normal";
            } elseif ($this->gal_gt_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->gal_gt_value !== null) {
            $result .= " ($this->gal_gt_value)";
        }
        
        return $result;
    }

    public function getDbBDiagnoseAttribute()
    {
        $result = '';
        if ($this->db_b_result !== null) {
            if ($this->db_b_result === 'N') {
                $result .= "Normal";
            } elseif ($this->db_b_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->db_b_value !== null) {
            $result .= " ($this->db_b_value)";
        }
        
        return $result;
    }

    public function getFqIrtDiagnoseAttribute()
    {
        $result = '';
        if ($this->fq_irt_result !== null) {
            if ($this->fq_irt_result === 'N') {
                $result .= "Normal";
            } elseif ($this->fq_irt_result === 'S') {
                $result .= "Alterado";
            }
        }
        if ($this->fq_irt_value !== null) {
            $result .= " ($this->fq_irt_value)";
        }
        
        return $result;
    }
}
